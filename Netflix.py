#!/usr/bin/env python3

# -------
# imports
# -------

from math import sqrt
import pickle
from requests import get
from os import path
from numpy import sqrt, square, mean, subtract
import random


def create_cache(filename):
    """
    filename is the name of the cache file to load
    returns a dictionary after loading the file or pulling the file from the public_html page

    The create_cache method loads a cache of data by taking a pickle file
    (basically a compressed python dictionary) and unpacking it, so its data
    can be accessed in a dictionary in the rest of the Python program.

    It gets the pickle file by loading from a web address. In this case,
    the web address is the public repository of caches for this class.

    Accepts: filename (-> web address -> pickle file)
    Returns: cache (as dict) 

    """
    cache = {}
    filePath = "/u/fares/public_html/netflix-caches/" + filename

    if path.isfile(filePath):
        with open(filePath, "rb") as f:
            cache = pickle.load(f)
    else:
        webAddress = "http://www.cs.utexas.edu/users/fares/netflix-caches/" + \
            filename
        bytes = get(webAddress).content
        cache = pickle.loads(bytes)

    return cache

# using these 4 given caches + 1 given value:
AVERAGE_RATING = 3.60428996442
ACTUAL_RATING = create_cache(
    "cache-actualCustomerRating.pickle")
MOVIE_AVG = create_cache(
    "cache-averageMovieRating.pickle")
YEAR_OF_RATING = create_cache(
    "cache-yearCustomerRatedMovie.pickle")
CUSTOMER_AVG_BY_YEAR = create_cache(
    "cache-customerAverageRatingByYear.pickle")

# ------------
# netflix_eval
# ------------

def netflix_eval(reader, writer) :
    """
    The netflix_eval function reads in a file of movie & customer IDs,
    accesses caches to pull relevant data, and makes a prediction of a rating
    for each movie-customer ID pair.

    The prediction is made by averaging the movie overall average rating +
    the customer's average ratings for the year they're rating the movie.

    For optimization, there is a scale factor added to make up some of the
    difference when the customer's average rating is far above the movie's
    average rating.
    We found that other optimizations (such as trending the prediction towards
    the movie's average rating, or trying to account for high or low skew
    of predictions overall) helped in unit tests, but resulted in a higher
    RSME for the overall probe data set, and thus were undesirable for this
    set data input.

    RMSE is the root mean square error. It is a representation of how close
    our predictions are to the true ratings. The lower, the better. Our goal
    was to get the RMSE of the entire probe.txt data set below 1.00. Ours sits
    at 0.98.

    Accepts: reader & writer (-> reads in movie and customer IDs from a text file)
    Returns: none (instead, predictions and final RMSE are written to an output file)

    """
    predictions = []
    actual = []

    # iterate throught the file reader line by line
    for line in reader:
    # need to get rid of the '\n' by the end of the line
        line = line.strip()
        # check if the line ends with a ":", i.e., it's a movie title 
        if line[-1] == ':':
            # It's a movie
            current_movie = line.rstrip(':')
            
            movieAvg = MOVIE_AVG[int(current_movie)]
            
            writer.write(line)
            writer.write('\n')
        else:
	    # It's a customer
            current_customer = line
            prediction = AVERAGE_RATING #default value

            year = YEAR_OF_RATING[ (int(current_customer), int(current_movie)) ]
            cYrAvg = CUSTOMER_AVG_BY_YEAR[ (int(current_customer), int(year)) ]

            if cYrAvg == 5.0:           #if all ratings are 5, this one must also be 5
                prediction = 5.0
            elif cYrAvg == 1.0:         #if all ratings are 1, this one must also be 1
                prediction = 1.0
            else:
                prediction = (movieAvg + cYrAvg) / 2

                # -- Scale Factor -- #
                gap = cYrAvg - movieAvg
                if gap >= 1.1:          #if the customer tends to rate movies much higher
                    prediction += 0.3
               
            predictions.append(prediction)
            
            rating = ACTUAL_RATING[ (int(current_customer), int(current_movie))]
            actual.append(rating)
            
            writer.write(str( '%.1f'%(prediction) ))
            writer.write('\n')

    # calculate rmse for predictions and actuals
    rmse = sqrt(mean(square(subtract(predictions, actual))))
    writer.write(str( '%.2f'%(rmse) )[:4] + '\n')

